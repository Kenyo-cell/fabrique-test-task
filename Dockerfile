FROM arm64v8/openjdk:17
WORKDIR /application
COPY target/Fabrique-test-task-1.0-SNAPSHOT.jar fabrique-test-app.jar
EXPOSE 8080
CMD [ "java", "-jar", "fabrique-test-app.jar" ]

