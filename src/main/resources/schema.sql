CREATE TABLE IF NOT EXISTS distributions(
    id SERIAL PRIMARY KEY,
    start_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "message" TEXT NOT NULL,
    "filter" TEXT NOT NULL,
    end_time TIMESTAMPTZ DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS clients(
    id SERIAL PRIMARY KEY,
    phone_number CHAR(11) NOT NULL,
    operator_code CHAR(3) GENERATED ALWAYS AS ( SUBSTRING(phone_number, 2, 3) ) STORED,
    "tag" TEXT NOT NULL DEFAULT 'any',
    "timezone" VARCHAR(6) NOT NULL DEFAULT '+0'
);

CREATE TABLE IF NOT EXISTS messages(
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    status TEXT NOT NULL DEFAULT 'NOT_SEND',
    distribution_id SERIAL NOT NULL,
    client_id SERIAL NOT NULL,
    CONSTRAINT distribution_id_fk
    FOREIGN KEY (distribution_id) REFERENCES distributions (id)
                                   ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT client_id_fk
    FOREIGN KEY (client_id) REFERENCES clients (id)
                                    ON DELETE CASCADE ON UPDATE CASCADE
);