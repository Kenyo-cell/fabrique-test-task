package org.example.response.object;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CommonDistributionStatsEntity {
    @Id
    long id;
    OffsetDateTime startTime;
    String message;
    String status;
    int count;
}
