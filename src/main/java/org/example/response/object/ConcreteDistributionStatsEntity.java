package org.example.response.object;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SqlResultSetMapping(
        name = "CommonDistributionStatsMapping",
        classes = @ConstructorResult(
                targetClass = CommonDistributionStatsEntity.class,
                columns = {
                        @ColumnResult(name = "message_id", type = Long.class),
                        @ColumnResult(name = "client_id", type = Long.class),
                        @ColumnResult(name = "created_at", type = OffsetDateTime.class),
                        @ColumnResult(name = "status", type = String.class),
                        @ColumnResult(name = "phone_number", type = String.class)
                }
        )
)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ConcreteDistributionStatsEntity {
    @Id
    long messageId;
    long clientId;
    OffsetDateTime createdAt;
    String status;
    String phoneNumber;
}
