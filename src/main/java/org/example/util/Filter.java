package org.example.util;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = false)
@NoArgsConstructor
@AllArgsConstructor
public class Filter {
    String tag = "%";
    String operatorCode = "%";
}
