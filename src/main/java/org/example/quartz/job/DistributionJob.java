package org.example.quartz.job;

import lombok.SneakyThrows;
import org.example.service.JobService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class DistributionJob implements Job {
    private final JobService service;

    public DistributionJob(JobService service) {
        this.service = service;
    }

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        long id = jobExecutionContext.getJobDetail().getJobDataMap().getLong(JobService.ID_KEY);

        service.distributeMessages(id);
    }
}
