package org.example.quartz.job;

import lombok.RequiredArgsConstructor;
import org.example.entity.DistributionEntity;
import org.example.repository.DistributionRepository;
import org.example.service.JobService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import java.time.OffsetDateTime;
import java.util.List;

@RequiredArgsConstructor
public class UpdateJob implements Job {
    private final DistributionRepository repository;
    private final JobService service;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<DistributionEntity> distributions = repository.getAllByStartTimeBeforeAndEndTimeAfter(
                OffsetDateTime.now().plusHours(1), OffsetDateTime.now()
        );

        distributions.forEach(distribution -> {
            try {
                service.registerJob(distribution);
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        });
    }
}
