package org.example.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.example.entity.DistributionEntity;
import org.example.repository.DistributionRepository;
import org.example.response.object.CommonDistributionStatsEntity;
import org.example.response.object.ConcreteDistributionStatsEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.OffsetDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DistributionService {
    private final DistributionRepository repository;
    private final JobService jobService;
    @PersistenceContext
    private final EntityManager entityManager;

    @SneakyThrows
    public void save(DistributionEntity distribution) {
        saveOrUpdateDistribution(distribution);
    }

    public void update(long id, DistributionEntity distribution) throws IllegalArgumentException {
        if (!repository.existsById(id)) throw new IllegalArgumentException("Distribution does not exists");

        distribution.setId(id);
        saveOrUpdateDistribution(distribution);
    }

    public void delete(long id) throws IllegalArgumentException {
        if (!repository.existsById(id)) throw new IllegalArgumentException("Distribution does not exists");
        repository.deleteById(id);
    }

    public void validateDistribution(DistributionEntity distribution) throws IllegalArgumentException {
        if (distribution.getStartTime().isAfter(distribution.getEndTime()))
            throw new IllegalArgumentException("End Time must contain date after the Start Time");
    }

    @SneakyThrows
    public void checkAndSetOnExecution(DistributionEntity distribution) {
        OffsetDateTime now = OffsetDateTime.now();
        if (now.isAfter(distribution.getStartTime().plusHours(1)) && now.isBefore(distribution.getEndTime())) {
            jobService.registerJob(distribution);
        }
    }

    private void saveOrUpdateDistribution(DistributionEntity distribution) {
        repository.save(distribution);
        validateDistribution(distribution);
        checkAndSetOnExecution(distribution);
    }

    public List<CommonDistributionStatsEntity> getCommonStats() {
        String query = "select d.id as id, d.message as message, d.start_time as start_time, status, " +
                "count(m.distribution_id = d.id) from messages m, distributions d " +
                "where m.distribution_id = d.id group by (d.id, d.message, d.start_time, status)";

        return entityManager.createNativeQuery(query, CommonDistributionStatsEntity.class).getResultList();
    }

    public List<ConcreteDistributionStatsEntity> getDistributionStatsById(long id) {
        String query = String.format("select m.id as message_id, c.id as client_id, m.created_at as created_at, " +
                "m.status as status, c.phone_number as phone_number from messages m " +
                "join clients c on c.id = m.client_id where m.distribution_id = %d " +
                "group by (m.id, c.id, m.created_at, m.status, c.phone_number, m.distribution_id)", id);
        return entityManager.createNativeQuery(query, ConcreteDistributionStatsEntity.class).getResultList();
    }
}
