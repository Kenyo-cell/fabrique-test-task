package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.entity.ClientEntity;
import org.example.repository.ClientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository repository;

    @Transactional
    public void save(ClientEntity client) {
        repository.saveClient(client.getPhoneNumber(), client.getTag(), client.getTimezone());
    }

    @Transactional
    public void update(long id, ClientEntity client) throws IllegalArgumentException {
        if (!repository.existsById(id)) throw new IllegalArgumentException("User does not exists");

        repository.updateClient(id, client.getPhoneNumber(), client.getTag(), client.getTimezone());
    }

    public void delete(long id) throws IllegalArgumentException {
        if (!repository.existsById(id)) throw new IllegalArgumentException("User does not exists");

        repository.deleteById(id);
    }
}
