package org.example.service;

import org.example.entity.MessageEntity;
import org.example.repository.MessageRepository;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    private final MessageRepository repository;

    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    public MessageEntity save(MessageEntity entity) {
        return repository.save(entity);
    }
}
