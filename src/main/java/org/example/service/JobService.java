package org.example.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.entity.ClientEntity;
import org.example.entity.DistributionEntity;
import org.example.entity.MessageEntity;
import org.example.entity.Status;
import org.example.request.object.Message;
import org.example.quartz.job.DistributionJob;
import org.example.repository.ClientRepository;
import org.example.repository.DistributionRepository;
import org.example.repository.MessageRepository;
import org.example.util.Filter;
import org.quartz.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.web.client.RestTemplate;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class JobService {
    public static final String ID_KEY = "distribution";

    private final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzk3NTE1NTQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ik1hcmNvbWVkaWFNb2JpbGl0eSJ9.szZSwEFAE0AIn6LYu4OmjbayqQGGwAcbTrJrLpcWn6o";
    private final String URL = "https://probe.fbrq.cloud/v1/send/";
    private final HttpHeaders headers = new HttpHeaders(
            new MultiValueMapAdapter<>(
                    Map.of("Authorization", List.of("Bearer " + TOKEN))
            )
    );

    private final Scheduler scheduler;
    private final ClientRepository clientRepository;
    private final MessageRepository messageRepository;
    private final DistributionRepository distributionRepository;

    public void registerJob(DistributionEntity distribution) throws SchedulerException {
        JobDetail jobDetail = JobBuilder.newJob(DistributionJob.class)
                .usingJobData(ID_KEY, distribution.getId())
                .build();

        scheduler.scheduleJob(jobDetail, TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .startAt(Date.from(distribution.getStartTime().toInstant()))
                .build()
        );
    }

    @Transactional
    public void distributeMessages(long id) throws JsonProcessingException {
        DistributionEntity distribution = distributionRepository.getById(id);

        Filter filter = new ObjectMapper().readValue(
                distribution.getFilter(),
                Filter.class
        );

        List<ClientEntity> clients = clientRepository.getAllByTagIsLikeAndOperatorCodeIsLike(
                filter.getTag(),
                filter.getOperatorCode()
        );

        clients.forEach(
                client -> sendToClient(client, distribution)
        );
    }

    private void sendToClient(ClientEntity client, DistributionEntity distribution) {
        OffsetDateTime clientTrueTime = OffsetDateTime.now().withOffsetSameInstant(ZoneOffset.of(client.getTimezone()));
        if (!clientTrueTime.isAfter(distribution.getStartTime()) || !clientTrueTime.isBefore(distribution.getEndTime()))
            return;

        RestTemplate restTemplate = new RestTemplate();

        MessageEntity messageEntity = MessageEntity.builder()
                .client(client)
                .createdAt(OffsetDateTime.now())
                .status(Status.NOT_SEND)
                .distribution(distribution)
                .build();

        messageEntity = messageRepository.save(messageEntity);
        long messageId = messageEntity.getId();

        Message message = new Message(
                messageId,
                Long.parseLong(client.getPhoneNumber()),
                distribution.getMessage()
        );

        HttpEntity<?> httpEntity = new HttpEntity<>(message, headers);
        ResponseEntity<?> response = restTemplate.postForEntity(
                URL + messageId,
                httpEntity,
                Message.class
        );

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            messageEntity.setStatus(Status.SEND);
            messageRepository.save(messageEntity);
        }
    }
}
