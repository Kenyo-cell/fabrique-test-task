package org.example.repository;

import org.example.entity.DistributionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface DistributionRepository extends JpaRepository<DistributionEntity, Long> {
    List<DistributionEntity> getAllByStartTimeBeforeAndEndTimeAfter(OffsetDateTime startAfter, OffsetDateTime endBefore);
}
