package org.example.repository;

import org.example.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    @Modifying
    @Query(value = "INSERT INTO clients (phone_number, tag, timezone) VALUES (:phone, :tag, :tz);", nativeQuery = true)
    void saveClient(@Param("phone") String phone, @Param("tag") String tag, @Param("tz") String timezone);

    @Modifying
    @Query(value = "UPDATE clients SET (phone_number, tag, timezone) = (:phone, :tag, :tz) WHERE id = :id ;", nativeQuery = true)
    void updateClient(@Param("id") long id, @Param("phone") String phone,
                      @Param("tag") String tag, @Param("tz") String timezone);

    List<ClientEntity> getAllByTagIsLikeAndOperatorCodeIsLike(String tag, String operatorCode);
}
