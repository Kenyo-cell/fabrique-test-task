package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "clients")
public class ClientEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "phone_number")
    @Pattern(regexp = "^7\\d{10}$", message = "Phone Number must match 7XXXXXXXXXX, where X is number")
    private String phoneNumber;

    private String operatorCode;

    @Column
    private String tag;

    @Column
    @Pattern(regexp = "^[+\\-]0?([0-9]|10|11|12)(:[0-5]?[0-9])?$", message = "TimeZone must ")
    private String timezone;
}
