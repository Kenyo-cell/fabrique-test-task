package org.example.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "distributions")
public class DistributionEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_time")
    @JsonFormat(pattern = "dd/MM/yy HH:mm x", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime startTime;

    @Column
    private String message;

    @Column
    private String filter;

    @Column(name = "end_time")
    @JsonFormat(pattern = "dd/MM/yy HH:mm x", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime endTime;

    @JsonProperty("filter")
    public void setFilter(JsonNode filter) {
        this.filter = filter.toString();
    }
}
