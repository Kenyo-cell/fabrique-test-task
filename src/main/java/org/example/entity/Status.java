package org.example.entity;

public enum Status {
    NOT_SEND("not send"),
    SEND("send");

    private String alias;

    Status(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
