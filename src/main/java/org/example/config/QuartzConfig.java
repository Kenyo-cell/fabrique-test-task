package org.example.config;

import org.example.quartz.job.UpdateJob;
import org.quartz.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
@EnableAutoConfiguration
public class QuartzConfig {
    @Bean
    public Scheduler scheduler(SchedulerFactoryBean factory, JobDetail job) throws SchedulerException {
        Scheduler scheduler = factory.getScheduler();

        scheduler.scheduleJob(this.hourlyUpdateTrigger(job));
        scheduler.start();

        return scheduler;
    }

    @Bean
    public JobDetail updateDistributionJobs() {
        return JobBuilder.newJob(UpdateJob.class)
                .withIdentity("hourlyUpdateJob")
                .storeDurably()
                .build();
    }

    public Trigger hourlyUpdateTrigger(JobDetail job) {
        return TriggerBuilder.newTrigger()
                .withIdentity("hourlyUpdateTrigger")
                .startNow()
                .forJob(job)
                .withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .repeatForever()
                                .withIntervalInHours(1)
                )
                .build();
    }
}
