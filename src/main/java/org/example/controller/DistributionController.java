package org.example.controller;

import lombok.SneakyThrows;
import org.example.entity.DistributionEntity;
import org.example.response.object.CommonDistributionStatsEntity;
import org.example.response.object.ConcreteDistributionStatsEntity;
import org.example.service.DistributionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("distribution")
public class DistributionController {
    private final DistributionService service;

    public DistributionController(DistributionService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody DistributionEntity distribution) {
        service.save(distribution);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SneakyThrows
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable long id, @RequestBody DistributionEntity distribution) {
        service.update(id, distribution);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/stats")
    public List<CommonDistributionStatsEntity> getCommonStats() {
        return service.getCommonStats();
    }

    @GetMapping("/stats/{id}")
    public List<ConcreteDistributionStatsEntity> getStatsById(@PathVariable long id) {
        return service.getDistributionStatsById(id);
    }
}
