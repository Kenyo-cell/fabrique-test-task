openapi: 3.0.3
info:
  title: Api for Fabrique Test Task App
  description: Simple openapi specification for test task
  version: 1.0-SNAPSHOT
servers:
  - url: 'http://localhost:8080'

components:
  parameters:
    id:
      in: path
      name: id
      required: true
      schema:
        type: integer
  responses:
    '200':
      description: created
    '400':
      description: input error
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/Error"
    '500':
      description: internal server error
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/Error"

  schemas:
    Client:
      type: object
      properties:
        phoneNumber:
          type: string
          pattern: "7XXXXXXXXXX"
        tag:
          type: string
        timezone:
          type: string

    Distribution:
      type: object
      properties:
        startTime:
          type: string
          format: timestamp with timezone
          pattern: "dd/MM/yy HH:mm x"
        message:
          type: string
        filter:
          type: object
          properties:
            tag:
              type: string
            operatorCode:
              type: string
        endTime:
          type: string
          format: timestamp with timezone
          pattern: "dd/MM/yy HH:mm x"


    Error:
      type: object
      properties:
        message:
          type: string




paths:
  /client:
    post:
      summary: returns nothing and saves client entity
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Client"

      responses:
        '200':
          $ref: '#/components/responses/200'
        '400':
          $ref: '#/components/responses/400'
        '500':
          $ref: '#/components/responses/500'

  /client/{id}:
    put:
      parameters:
        - $ref: '#/components/parameters/id'
      summary: returns nothing and updates client entity
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Client"

      responses:
        '200':
          $ref: '#/components/responses/200'
        '400':
          $ref: '#/components/responses/400'
        '500':
          $ref: '#/components/responses/500'
    delete:
      parameters:
        - $ref: '#/components/parameters/id'
      summary: returns nothing and delete client entity
      responses:
        '200':
          $ref: '#/components/responses/200'
        '400':
          $ref: '#/components/responses/400'
        '500':
          $ref: '#/components/responses/500'

  /distribution:
    post:
      summary: returns nothing and saves distribution entity
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Distribution"
      responses:
        '200':
          $ref: "#/components/responses/200"
        '400':
          $ref: "#/components/responses/400"
        '500':
          $ref: "#/components/responses/500"
  /distribution/{id}:
    put:
      parameters:
        - $ref: "#/components/parameters/id"
      summary: returns nothing and updates distribution entity
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Distribution"
      responses:
        '200':
          $ref: "#/components/responses/200"
        '400':
          $ref: "#/components/responses/400"
        '500':
          $ref: "#/components/responses/500"
    delete:
      parameters:
        - $ref: "#/components/parameters/id"
      summary: returns nothing and delete distribution entity
      responses:
        '200':
          $ref: "#/components/responses/200"
        '400':
          $ref: "#/components/responses/400"
        '500':
          $ref: "#/components/responses/500"

  /stats:
    get:
      summary: returns an array of objects
      responses:
        '200':
          description: success response from db
          content:
            application/json:
              schema:
                type: array
                items:
                  properties:
                    id:
                      type: integer
                    startTime:
                      type: string
                      format: timestamp with timezone
                      pattern: "dd/MM/yy HH:mm x"
                    message:
                      type: string
                    status:
                      type: string
                    count:
                      type: integer

        '400':
          $ref: "#/components/responses/400"
        '500':
          $ref: "#/components/responses/500"

  /stats/{id}:
    get:
      parameters:
        - $ref: "#/components/parameters/id"
      description: returns an list of objects describing each message to client relation
      responses:
        '200':
          description: success response
          content:
            application/json:
              schema:
                type: array
                items:
                  properties:
                    messageId:
                      type: integer
                    clientId:
                      type: integer
                    createdAt:
                      type: string
                      format: timestamp with timezone
                      pattern: "dd/MM/yy HH:mm x"
                    status:
                      type: string
                    phoneNumber:
                      type: string